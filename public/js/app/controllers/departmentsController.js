Caweb.controller('departmentsController', function($scope, $rootScope, CAService, $mdToast, $location, $mdDialog,
	UserService) {
	if($rootScope.user.role == "CLIENT") {
		$location.path('/clientArea/' + $rootScope.user.id);
		return;
	}
	$rootScope.selectedTab = $rootScope.tabsMap['departments'];
	function getDepartments() {
		CAService.getDepartments().then(function(departments){
			$scope.departments = departments;
		}, function(err) {
			$mdToast.show($mdToast.simple()
			.textContent("Unable to fetch departments")
			.position("top right")
			.hideDelay(5000));
		});
	}
	getDepartments();
	function showDialog(){
		$mdDialog.show({
	    	controller : function($scope, theScope) {
	    		$scope.theScope = theScope
	    		$scope.$watch('departmentForm', function() {
	    			$scope.theScope.departmentForm = $scope.departmentForm;
	    		}, true);
	    	},
			templateUrl : 'department.tmpl.html',
			parent : angular.element(document.body),
			clickOutsideToClose:true,
			locals : {
				theScope : $scope
			}
		}).then(function(){
		});
	}

	$scope.closeDialog = function() {
		$mdDialog.cancel();
		$scope.currentDepartment = {};
	}

	$scope.showAddBranchDialog = function() {
		$scope.dialogType = 'Add';
		showDialog();
	}

	var original;
	$scope.showUpdateBranchDialog = function(index) {
		$scope.dialogType = 'Edit';
		original = $scope.branches[index];
		$scope.currentDepartment = angular.copy($scope.branches[index]);
		showDialog();
	}

	$scope.branchAction = function() {
		if(!$scope.currentDepartment.name && !$scope.currentDepartment.address && !$scope.currentDepartment.office_landline) {
			return;
		}
		if($scope.dialogType == 'Add') {
			var payload = {
				name : $scope.currentDepartment.name,
				address : $scope.currentDepartment.address,
				office_landline : $scope.currentDepartment.office_landline
			};
			CAService.createBranch(payload).then(function(){
				$mdToast.show($mdToast.simple()
				.textContent("Deprtment has been Successfully added.")
				.position("top right")
				.hideDelay(5000));
				getDepartments();
				$scope.closeDialog();
			}, function(err) {
				$mdToast.show($mdToast.simple()
				.textContent("Unable to add Department.")
				.position("top right")
				.hideDelay(5000));
			});
		} else if($scope.dialogType == 'Edit') {
			var departmentAction = CAService.editUser;
			var payload = CAService.calculateDiff($scope.currentDepartment, original);
			if(!angular.equals(payload,{})) {
				CAService.updateBranch($scope.currentDepartment.id, payload).then(function(){
					$mdToast.show($mdToast.simple()
					.textContent("Branch has been Successfully updated.")
					.position("top right")
					.hideDelay(5000));
					getBranches();
					$scope.closeDialog();
				}, function(err) {
					$mdToast.show($mdToast.simple()
					.textContent("Unable to update the branch.")
					.position("top right")
					.hideDelay(5000));
				});
			}
		}
	}

	$scope.showRemoveBranchDialog = function(index) {
		var confirm = $mdDialog.confirm()
	          .title('Remove Branch - ' + $scope.branches[index].name)
	          .textContent('Are You sure you want to remove the Branch?')
	          .ariaLabel('Remove Branch')
	          .ok('Remove')
	          .cancel('Cancel');
	    $mdDialog.show(confirm).then(function() {
	    	CAService.removeBranch($scope.branches[index].id, index).then(function() {
	    		$scope.branches.splice(index,1);
	    		$mdToast.show($mdToast.simple()
				.textContent("Removed the Branch Successfully.")
				.position("top right")
				.hideDelay(5000));
	    	}, function(err) {
	    		$mdToast.show($mdToast.simple()
				.textContent("Error in removing Brnach.")
				.position("top right")
				.hideDelay(5000));
	    	});
	    }, function() {
	    });
	}
});
